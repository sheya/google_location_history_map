import os
import sys
import json
import folium
import sqlite3


def main():
    if len(sys.argv) < 2:
        print('Usage: python plot infile.json outfile.html')

    global db
    global cur

    db = sqlite3.connect('location_history.db')
    cur = db.cursor()

    infile, outfile = sys.argv[1], sys.argv[2] if len(sys.argv) == 3 else 'map.html'

    if not os.path.exists(infile):
        print('Error: Invalid infile specified')
        exit()

    load_db(infile)
    plot(outfile)
    print('All done!')


def number_string(n):
    return '{:,}'.format(n)


def load_db(infile):
    print('Loading file')

    with open(infile) as f:  # file from google archive manager
        data = json.load(f)

    location_count = len(data.get('locations'))

    if not location_count:
        print('Error: Invalid infile specified')
        exit()

    print('Found {} locations, inserting into database'.format(number_string(location_count)))

    cur.execute('''create table if not exists locations (
          id        INTEGER primary key,
          lat       REAL,
          lon       REAL
        );''')

    cur.execute('delete from locations')
    db.commit()

    for i, l in enumerate(data['locations']):
        if i % 10000 == 0:
            print('Inserting {} of {} locations into database'.format(number_string(i), number_string(location_count)),
                  end='\r')
        latitudeE7, longitudeE7 = l['latitudeE7'], l['longitudeE7']
        if latitudeE7 > 900000000: latitudeE7 = latitudeE7 - 4294967296  # takeout bug
        if longitudeE7 > 1800000000: longitudeE7 = longitudeE7 - 4294967296  # takeout bug
        lat, lon = latitudeE7 / 1e7, longitudeE7 / 1e7
        db.execute('insert into locations (lat, lon) values ({}, {})'.format(lat, lon))

    print('\nCommitting to database')

    db.commit()
    print('Loaded to database')


class Reg(object):
    def __init__(self, cursor, row):
        for (attr, val) in zip((d[0] for d in cursor.description), row):
            setattr(self, attr, val)


def clean_row(cursor, cr):
    return Reg(cursor, cr)


def plot(outfile):
    layer = folium.Map(
        zoom_start=3,
        prefer_canvas=True,
        tiles='CartoDB dark_matter',
        location=[51.5737838, -0.06512889999999061],
    )

    i = 0
    print('Fetching points from database')
    sql = 'select distinct round(lat, 2) as lat, round(lon, 2) as lon from locations'
    count = cur.execute('select count(*) from ({})'.format(sql)).fetchone()[0]

    cur.execute('{}'.format(sql))
    for r in cur:
        i += 1
        if i % 10000 == 0:
            print('Adding marker {} of {}'.format(number_string(i), number_string(count)), end='\r')
        row = clean_row(cur, r)
        folium.CircleMarker(
            radius=1,
            location=[row.lat, row.lon],
        ).add_to(layer)

    print('\nSaving map')
    layer.save(outfile)

    print('Finished generating map')


if __name__ == '__main__':
    main()
