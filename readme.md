### Plot Google location history on an interactive map

`Usage: python plot.py 'input.json' 'output.html'`

1. Go to Google Takeout: https://takeout.google.com/settings/takeout
2. Deselect all, and scroll down to Location History, select it , and click 'Next Step'
3. It might take a while, but you'll get to download a zip file that will include 'Location History.json'
4. Unzip and pass the JSON file to this program

#### Requirements:
* Folium: `pip install folium`

#### Considerations
I had over 1.5 million data points in my location history, so this script narrows points down to coordinates with two decimal places - an accuracy of about 1 km. Feel free to change this in line 92. There will also inevitable be points for locations that you have never visited, generated from when you use a VPN etc. A reverse geocoder can be used to filter out such points.

Folium supports different themes too, consider [reading the docs](https://python-visualization.github.io/folium/) and editing lines 83-88

![](sample.png)